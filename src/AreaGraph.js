import React from "react";
import PropTypes from "prop-types";
import { AreaClosed } from "@vx/shape";
import { Group } from "@vx/group";
import { scaleTime, scaleLinear } from "@vx/scale";
import { AxisLeft, AxisRight, AxisBottom } from "@vx/axis";
import { withScreenSize } from "@vx/responsive";
import { extent, max } from "d3-array";
import tinycolor from "tinycolor2";

var data = [
  { date: new Date(2007, 3, 24), output: 45.24, something: 12.0 },
  { date: new Date(2007, 3, 25), output: 65.35, something: 15.0 },
  { date: new Date(2007, 3, 26), output: 75.84, something: 21.0 },
  { date: new Date(2007, 3, 27), output: 99.92, something: 16.0 },
  { date: new Date(2007, 3, 30), output: 87.8, something: 19.0 },
  { date: new Date(2007, 4, 1), output: 65.47, something: 25.0 }
];

const DataGraph = props => {
  const width = props.screenWidth * 0.8;
  const height = props.screenHeight * 0.8;

  const margin = {
    top: 10,
    bottom: 40,
    left: 55,
    right: 55
  };
  const xMax = width - margin.left - margin.right;
  const yMax = height - margin.top - margin.bottom;

  const x = d => d.date;
  const y = key => d => d[key];

  const xScale = scaleTime({
    range: [0, xMax],
    domain: extent(props.dataset, x)
  });

  const yScale = key =>
    scaleLinear({
      range: [yMax, 0],
      domain: [0, max(props.dataset, y(key))]
    });

  return (
    <div>
      <div>Graph Controls</div>
      <svg width={width} height={height}>
        <Group top={margin.top} left={margin.left}>
          <AxisLeft
            scale={yScale("output")}
            top={0}
            left={0}
            label={"Close Price ($)"}
            labelOffset={25}
            stroke={"#1b1a1e"}
            tickTextFill={"#1b1a1e"}
          />
          <AxisRight
            scale={yScale("something")}
            top={0}
            left={xMax}
            label={"Close Price ($)"}
            labelOffset={25}
            stroke={"#1b1a1e"}
            tickTextFill={"#1b1a1e"}
          />
          <AxisBottom
            scale={xScale}
            top={yMax}
            label={"Years"}
            stroke={"#1b1a1e"}
            tickTextFill={"#1b1a1e"}
          />
          <AreaClosed
            data={props.dataset}
            xScale={xScale}
            yScale={yScale("output")}
            x={x}
            y={y("output")}
            strokeWidth={0}
            fill={tinycolor("red")
              .setAlpha(0.75)
              .toRgbString()}
          />
          <AreaClosed
            data={props.dataset}
            xScale={xScale}
            yScale={yScale("something")}
            x={x}
            y={y("something")}
            strokeWidth={0}
            fill={tinycolor("green")
              .setAlpha(0.75)
              .toRgbString()}
          />
        </Group>
      </svg>
    </div>
  );
};

DataGraph.propTypes = {
  dataset: PropTypes.array.isRequired
};

DataGraph.defaultProps = {
  dataset: data
};

export default withScreenSize(DataGraph);
